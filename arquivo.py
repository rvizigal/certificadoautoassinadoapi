def cria_arquivo(countryName, stateOrProvinceName, localityName, organizationalUnitName, commonName, emailAddress):
    # Abre o arquivo no modo de escrita ('w' significa write)
    nome_arquivo = "openssl.cnf"

    conteudo = f"""[ req ]
default_bits = 2048
prompt = no
default_md = sha256
distinguished_name = req_distinguished_name
x509_extensions = v3_ca

[req_distinguished_name]
C = {countryName}
ST = {stateOrProvinceName}
L = {localityName}
O = {organizationalUnitName}
OU = TI
CN = {commonName}
emailAddress = {emailAddress}

[ v3_ca ]
subjectAltName = @alt_names

[ alt_names ]
DNS.1 = {commonName}.example.com
"""

    # Abre o arquivo no modo de escrita ('w' significa write)
    with open(nome_arquivo, 'w') as arquivo:
        arquivo.write(conteudo)

    return nome_arquivo