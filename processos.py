import subprocess

def execute_powershell_commands(commands):
    try:
        full_command = ";".join(commands)
        result = subprocess.run(
            ['powershell', '-Command', full_command], capture_output=True, text=True)
        if result.returncode == 0:
            return True, result.stdout
        else:
            return False, result.stderr
    except Exception as e:
        return False, str(e)


def cria_certificado(days,name):
    commands_to_execute = [
        f"openssl req -x509 -new -key chave_privada.key -out certificate.crt -days {days} -config {name}",
    ]

    success, output = execute_powershell_commands(commands_to_execute)

    if success:
        print("Comandos executados com sucesso:")
        #print(output)
    else:
        print("Erro ao executar os comandos:")
        print(output)


def ler_arquivo(caminho_arquivo):
    try:
        with open(caminho_arquivo, 'r') as arquivo:
            conteudo = arquivo.read()
            return conteudo
    except Exception as e:
        return str(e)
    
def cria_chave():
    commands_to_execute = [
        "openssl genpkey -algorithm RSA -out chave_privada.key"
    ]

    # commands_to_execute = ["openssl"]
    execute_powershell_commands(commands_to_execute)