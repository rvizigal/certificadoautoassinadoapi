import arquivo as arq
import processos as pr
from flask import Flask, request, jsonify
from pathlib import Path

app = Flask(__name__)

@app.route('/api/certificado', methods=['POST'])
def post():
    try:
        data = request.json  # Obtém o JSON dos parâmetros da requisição POST
        countryName = data.get('countryName')
        stateOrProvinceName = data.get('stateOrProvinceName')
        localityName = data.get('localityName')
        organizationalUnitName = data.get('organizationalUnitName')
        commonName = data.get('commonName')
        emailAddress = data.get('emailAddress')
        days = data.get('days')

        if countryName is None or stateOrProvinceName is None or localityName is None or organizationalUnitName is None or commonName is None or emailAddress is None:
            return jsonify({'error': 'Os campos countryName, stateOrProvinceName, localityName, organizationalUnitName, commonName, emailAddress são obrigatórios'}), 400

        
        name_config = arq.cria_arquivo(countryName, stateOrProvinceName, localityName,
                         organizationalUnitName, commonName, emailAddress)
        
        caminho = Path("./chave_privada.key")
        if not caminho.is_file():
            pr.cria_chave()
        
        pr.cria_certificado(days, name_config)
        certificado = pr.ler_arquivo("./certificate.crt")

        #sucesso
        resposta = {'status': 'Sucesso', 'certificado': certificado}

        return jsonify(resposta), 201

    except Exception as e:
        return jsonify({'error': 'Erro ao processar a requisição'}), 500


@app.route('/', methods=['GET'])
def get():
    resposta = {'status': 'Sucesso', 'Message': 'Service available'}
    return jsonify(resposta), 200

if __name__ == '__main__':
    app.run(debug=True)

