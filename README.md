# certificadoAutoassinadoAPI

## Aplicação de Criação de Certificado Autoassinado com OpenSSL

Esta aplicação em Python utiliza a biblioteca OpenSSL para criar certificados autoassinados. Ela oferece uma API com um endpoint para gerar certificados utilizando o método POST.

### Funcionalidades

- **Criação de Certificado Autoassinado:** A aplicação permite a criação de certificados digitais autoassinados usando a biblioteca OpenSSL.
- **Endpoint:** Através de um endpoint, você pode enviar um JSON contendo os parâmetros necessários para a criação do certificado com dias de validade.
  

**Exemplo de JSON de parâmetros:**
```json
POST - {host}/api/certificado
{
    "countryName": "BR",
    "stateOrProvinceName": "SP",
    "localityName": "Sao Paulo",
    "organizationalUnitName": "Departamento de TI",
    "commonName": "Certificado do Ramon",
    "emailAddress": "contato@example.com",
    "days" : 10
}
```

# Instalação de Imports para a Aplicação Python

Aqui está um guia passo a passo para instalar os imports necessários para a sua aplicação Python. Neste exemplo, vamos instalar os seguintes imports:

- `subprocess`
- `flask`
- `pathlib`

#

# Dependencias para execução do código

## Instalação dos imports via pip
O pip é uma ferramenta essencial para qualquer desenvolvedor Python, pois simplifica o processo de gerenciamento de pacotes e bibliotecas, permitindo que você concentre-se em criar, desenvolver e aprimorar seus projetos sem se preocupar com as complexidades de dependências e instalações.

### Passo 1: Instalar o Flask
___
O Flask é um framework web leve para Python. Ele é usado para criar aplicativos web. Use o seguinte comando para instalar o Flask.

```bash
pip install Flask
```
### Passo 2: Instalar o Módulo subprocess
___
O módulo subprocess permite que você crie novos processos, conecte-se a seus descritores de arquivos e obtenha seus códigos de retorno. É útil para interagir com o sistema operacional.
```bash
pip install subprocess
```
### Passo 3: Instalar o Módulo pathlib
___
O módulo pathlib fornece classes para manipular facilmente caminhos de sistema de arquivos, seja no Windows, Linux ou outros sistemas.
```bash
pip install pathlib
```
___
## Instalação do OpenSSL no Windows

O OpenSSL é uma ferramenta de código aberto usada para trabalhar com criptografia e certificados digitais. Aqui está um guia passo a passo para instalar o OpenSSL em uma máquina Windows.

### Passo 1: Download do OpenSSL

1. Acesse o site oficial do OpenSSL: [https://www.openssl.org/](https://www.openssl.org/).

2. No menu "Downloads", escolha a versão apropriada para Windows. Recomenda-se escolher a versão mais recente, compatível com a arquitetura da sua máquina (32 ou 64 bits).

3. Baixe o instalador executável (`.exe`) para o seu sistema.
___
### Passo 2: Instalação do OpenSSL

1. Execute o instalador que você baixou.

2. Siga as instruções do instalador:

   - Escolha um diretório para a instalação. O diretório padrão geralmente é adequado.
   - Selecione os componentes que deseja instalar. Para a maioria dos casos, as opções padrão serão suficientes.
   - Configure as opções de instalação conforme necessário.

3. Conclua a instalação. O OpenSSL agora está instalado no seu sistema.
___
### Passo 3: Configuração das Variáveis de Ambiente

Para que o OpenSSL funcione corretamente na linha de comando, você precisa configurar as variáveis de ambiente.

1. Clique com o botão direito no ícone "Este Computador" e selecione "Propriedades".

2. Clique em "Configurações avançadas do sistema" na barra lateral esquerda.

3. Na guia "Avançado", clique no botão "Variáveis de Ambiente".

4. Na seção "Variáveis do Sistema", encontre a variável chamada "Path" e clique em "Editar".

5. Adicione o caminho para a pasta `bin` do diretório de instalação do OpenSSL ao final da lista de caminhos existentes. O caminho padrão geralmente é `C:\Program Files\OpenSSL-Win64\bin` ou `C:\Program Files (x86)\OpenSSL-Win32\bin`, dependendo da versão que você instalou.

6. Clique em "OK" para fechar todas as janelas de configuração.
___
## Verificação da Instalação

Para verificar se a instalação do OpenSSL foi bem-sucedida, abra um prompt de comando e digite o seguinte comando:

```bash
openssl version
```

## Conclusão

Após a finalização do processo, a aplicação criará automaticamente três arquivos na pasta raiz:

1. **openssl.cnf:** Este arquivo armazena as configurações específicas para a geração do certificado, incluindo informações como nome, país e email de autenticação.

2. **chave_privada.key:** Este arquivo contém a chave privada associada à assinatura do certificado. A chave privada é essencial para garantir a segurança e autenticidade do certificado.

3. **certificate.crt:** Este arquivo é o certificado assinado gerado pelo OpenSSL. Ele contém as informações do certificado juntamente com a assinatura digital, que é uma garantia da autenticidade dos dados.

Esses arquivos são cruciais para a validação e uso do certificado gerado. O arquivo `certificate.crt` em particular é o que será retornado como resposta da API, permitindo que os usuários façam uso do certificado de forma segura e confiável.

### Exemplo de retorno da API
```json
{
	"certificado": "-----BEGIN CERTIFICATE-----\nMIIEFDCCAvygAwIBAgIUVMh4VdZJUbVWC0A7oArRop6080swDQYJKoZIhvcNAQEL\nBQAwgZsxCzAJBgNVBAYTAkJSMQswCQYDVQQIDAJTUDESMBAGA1UEBwwJU2FvIFBh\ndWxvMRswGQYDVQQKDBJEZXBhcnRhbWVudG8gZGUgVEkxCzAJBgNVBAsMAlRJMR0w\nGwYDVQQDDBRDZXJ0aWZpY2FkbyBkbyBSYW1vbjEiMCAGCSqGSIb3DQEJARYTY29u\ndGF0b0BleGFtcGxlLmNvbTAeFw0yMzA4MTEyMjMwMjlaFw0yMzA4MTIyMjMwMjla\nMIGbMQswCQYDVQQGEwJCUjELMAkGA1UECAwCU1AxEjAQBgNVBAcMCVNhbyBQYXVs\nbzEbMBkGA1UECgwSRGVwYXJ0YW1lbnRvIGRlIFRJMQswCQYDVQQLDAJUSTEdMBsG\nA1UEAwwUQ2VydGlmaWNhZG8gZG8gUmFtb24xIjAgBgkqhkiG9w0BCQEWE2NvbnRh\ndG9AZXhhbXBsZS5jb20wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQC7\nYNhd9S3t+ZT39FKliNZedaYVwnZVFa1VB+y9966e+gVB8/Q2Vw5uE/dwdjZKf6Pt\n9coXZ6pbLWJdh0exf4vhLd6q+8UuuslDw38rrjw5ukaYmu01SPHRZaMVRTL/ul6i\n4qV6alo7lbLh1UsEb24fXRo1iM4XdLGR9ARCssWEYNX12i6tR6BdLx6hIRg+W9ZV\nFCz83r7AaGJS+MSw+EuQDWJvSWKgvtQflOp4682RSsy9dlj5oBB3qWs0brkCLfDW\nWidK3F/ejm2XXCPoTPl9pgiig9vlzpj/MhMnEq+tFbo2bXTJ4VDLxrwTJwLMt3sA\nM+QwFhgVGHQ6KYA58x9RAgMBAAGjTjBMMCsGA1UdEQQkMCKCIENlcnRpZmljYWRv\nIGRvIFJhbW9uLmV4YW1wbGUuY29tMB0GA1UdDgQWBBR0Qn0X18qtyblT/f83+yLz\nZTPHfjANBgkqhkiG9w0BAQsFAAOCAQEAlDXcMZBNHAVnfXaqFytx3URe5WaTuU7P\nsMp/5Tl+uotlRh+Z5tBSv5Ubb8qYpj8pgLXAPimAhKKbuRW8HU69UZUI+PVud1vM\n2xhKOxoyTpM1Aq/qNrcKnLccb1x6MJs9s+y7fFtlbcOvWAsWo5j1VQ+ttmbhmWmR\nVA7+Fod46h3sA2qN9AGfuO19pzpPKUZUVVHIh4H7CiatekOAIG/Ixwjg3Zxxpwnv\n7UrmzTV7HOqySf3+a68t+p162/39xVIaVM4z/35pXlpog6QQpLocyKDa8n/KVQo1\nsoCkfPSWPROplIvxlCDyejjNLoePu3qNKcu7WlaywfZ97YlyWsSxwA==\n-----END CERTIFICATE-----\n",
	"status": "Sucesso"
}
```
___
# Licença de Uso

**IMPORTANTE: LEIA ATENTAMENTE ANTES DE UTILIZAR ESTE SOFTWARE.**

## Termos e Condições

Este é um acordo legal entre você (doravante referido como "Usuário") e Ramon Vizigal (doravante referido como "Autor"), relacionado ao software fornecido juntamente com esta licença. Ao utilizar este software, você concorda com os seguintes termos e condições:

1. **Uso Pessoal:** Este software pode ser usado tanto para fins pessoais quanto acadêmicos.

2. **Modificações:** O Usuário tem permissão para modificar, adaptar e melhorar o software de acordo com suas necessidades.

3. **Distribuição:** O Usuário pode distribuir este software, desde que mantenha os créditos e esta licença intactos em todas as cópias distribuídas.

4. **Sem Garantias:** O software é fornecido "como está", sem garantias de qualquer tipo, expressas ou implícitas. O Autor não se responsabiliza por quaisquer danos decorrentes do uso deste software.

5. **Suporte:** Este software é oferecido sem suporte técnico ou garantia de atualizações contínuas por parte do Autor.

## Direitos Autorais

Este software e sua documentação são protegidos por direitos autorais. Todos os direitos não concedidos explicitamente nesta licença são reservados ao Autor.

## Entre em Contato

Se tiver dúvidas sobre este código ou licença, entre em contato com Ramon Vizigal através do [linkedin](https://www.linkedin.com/in/ramon-vizigal-90164131/)

---

**Agradeço por usar este software.**

Licença criada por Ramon Vizigal.
